#include <stdio.h>
#include <stdlib.h>

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
#include "timers.h"
#include "event_groups.h"

#include "Libs/HardwareEmu.h"
#include "Libs/ControlPanelClient.h"

/** INFORMATIONS SUR LES PERIPHERIQUES ET COMMANDES
---------------------------------------------------
'V' : Vitesse du moteur (0-50)
'D' : Angle de braquage des roues avant en 1/10° de degré (10 -> 1∞)
'T' : Azimut de la tourelle du télémètre en 1/10°
'R' : Lecture de l'Azimuth de la tourelle en 1/10°
'U' : Distance mesure par le télémètre en 1/100 de mètre (en cm)
'X' : Position absolue X en cm
'Y' : Position absolue Y en cm
'Z' : Position absolue Z en cm
'N' : Numéro de la voiture (en fonction de l'ordre de connection)
'E' : Lecture des evènements (cf Annexe 2)
'H' : Donne le temps de course actuel
'S' : Temps du tour précédent
'M' : Mode de course :
    8 bits de poids fort: 1 Attente, 2 course, 3 essais libres)
    8 bits de poids faible : numero de piste
'C' : Informations sur le dernier capteur touché :
    8 bits de poids faible : numéro du capteur
    8 bits de poids fort : couleur ('R','J' ou 'V')
'J' : Proposition d'un code de dévérouillage.
    Une valeur de 0 à 5 par quartet.
'j' : Récupération du résultat de dernier code envoyé.
    0x77 si aucun code n'a été soumis.
    <0 si la réponse n'est pas disponible.
    0x0a0b avec a-> nombre de couleurs bien placées et b -> couleurs présentes mais mal placées.
'I' : Définition du nom du véhicule.
    Doit débuter par le caractère '#' et entraine le chargement de la configuration de piste correspondant au nom du véhicule si le nom se termine par '*'
'K' : Téléportation de la voiture sur le troncon de piste N (correspondant au capteur vert numero N).
    Attention à n'utiliser que pour des tests, les scores sont invalidés !


Contenu du Capteur de couleur;
Bit 0 : Point de passage Vert, remis à zéro lors de la lecture du périphérique 'C'
    1 : Point de passage Jaune, remis à zéro lors de la lecture du périphérique 'C'
    2 : Point de passage Rouge, remis à zéro lors de la lecture du périphérique 'C'
    3 : Point de passage Bleu, remis à zéro lors de la lecture du périphérique 'C'
    4 : Point de passage Cyan, remis à zéro lors de la lecture du périphérique 'C'
    5 : non utilisés
    6 : Collision avec le sol, Remise à zéro au changement de piste.
    7 : Point de passage course (vert), remis à zéro lors de la lecture du périphérique 'C'
    8 : La piste à changé , remis à zéro lors de la lecture du périphérique 'M'
    9 : Le mode de course a changé , remis à zéro lors de la lecture du périphérique 'M'
   10 : non utilisé
   11 : Le dernier point de passage est atteint la course est finie , remis à zéro au changement du mode de course.
   12 : La voiture est sortie de la piste.
   13 : Utilisation de la fonction de téléportation. Classement Invalidé. Remis à zero au changement de piste ou du mode de course.
   14 : Faux départ -> destruction de la voiture , remise à zéro au changement du mode de course.
   15 : Le Feu est Vert
**/

SemaphoreHandle_t xSemaphore;
SemaphoreHandle_t xSemaphore1;
signed consigne;

QueueHandle_t xQueue_mesure, xQueue_angle,xQueue_commande_angle_telemetre,xQueue_commande_angle_roues;
TaskHandle_t htask_1;
TaskHandle_t htask_2;
TaskHandle_t htask_3;
TaskHandle_t htask_4;
TaskHandle_t htask_5;
TaskHandle_t htask_6;

void task_regulation_angle(void * queue){
int i;
char nom[]="#VROUM !*";
CanFrame cmd={{'I',0,0}};

CanFrame cmdvitesseRot={{'T',0,0}};

signed vitesseRot;
signed angleFinal = 45;
signed angleCurrent = 0;
xQueue_commande_angle_telemetre = xQueueCreate( 10, sizeof( CanFrame ) );



for(i=0;i<8 && nom[i];i++){
    cmd.data.val=nom[i];
    CanTxSendFrame(cmd);
    vTaskDelay(10);
}

while(1){
            char tmp[32];
            sprintf(tmp,"Angle: %d ",angleFinal);
            lcd_com(0x80);
            lcd_str(tmp);

            xQueueReceive( xQueue_angle, &( angleCurrent ), ( TickType_t ) 10 );
            cmdvitesseRot.data.val = angleFinal*10-angleCurrent;
            xQueueSend( xQueue_commande_angle_telemetre, &( cmdvitesseRot ), ( TickType_t ) 10 );
            vTaskDelay(10);
    }

}

void commande_angle_telemetre(void * queue){
    CanFrame cmdvitesseRot;
    while(1){
        if( xQueue_commande_angle_telemetre != 0 )
        {
            xQueueReceive( xQueue_commande_angle_telemetre, &( cmdvitesseRot ), ( TickType_t ) 10 );
            if(CanTxReady()){
                   CanTxSendFrame(cmdvitesseRot);
            }

        }
    }
}

void commande_angle_roues(void * queue){
    signed commande;
    CanFrame cmdvitesseRot;
    cmdvitesseRot.data.id = 'D';
    cmdvitesseRot.data.rtr = 0;
    vTaskDelay(5000);
    while(1){
        if( xQueue_commande_angle_roues != 0 )
        {
            xQueueReceive( xQueue_commande_angle_roues, &( commande ), ( TickType_t ) 10 );
            cmdvitesseRot.data.val = commande;
            CanTxSendFrame(cmdvitesseRot);

        }
    }
}


void recuperation_angle(void * queue){
    xQueue_angle = xQueueCreate( 10, sizeof( signed short ) );
    CanFrame requestAngle;
    CanFrame responseAngle;
    requestAngle.data.id = 'R';
    requestAngle.data.rtr = 1;
    while(1){
        CanTxSendFrame(requestAngle);
        vTaskDelay(10);
        CanRxReadFrame(&responseAngle);
         if( xQueue_angle != 0 )
        {
            xQueueSend( xQueue_angle,&(responseAngle.data.val),( TickType_t ) 10 );

        }
    }
}


void recuperation_mesure(void * queue){

    xQueue_mesure = xQueueCreate( 10, sizeof( signed ) );
    signed tmp;
    CanFrame requestDistance;
    CanFrame responseDistance;
    requestDistance.data.id = 'U';
    requestDistance.data.rtr = 1;
    vTaskDelay(5000);
    while(1){
        CanTxSendFrame(requestDistance);
        vTaskDelay(10);
        CanRxReadFrame(&responseDistance);
        tmp = (signed)responseDistance.data.val;
        if( xQueue_mesure != 0 )
        {
            xQueueSend( xQueue_mesure,&(tmp),( TickType_t ) 10 );

        }
    }
}

void task_regulation_distance(void * queue){


CanFrame avancer;
xQueue_commande_angle_roues = xQueueCreate( 10, sizeof( signed short ) );

signed commande,mesure, erreur, erreur_precedente;
float Kp;

CanFrame fixerAngle={{'T',0,0}};

Kp = 1;

int k=1;

    CanFrame requestDistance;
    CanFrame responseDistance;
    requestDistance.data.id = 'U';
    requestDistance.data.rtr = 1;

    CanFrame cmdvitesseRot;
    cmdvitesseRot.data.id = 'D';
    cmdvitesseRot.data.rtr = 0;


vTaskDelay(5000);

vTaskSuspend(htask_1);
vTaskSuspend(htask_4);
vTaskSuspend(htask_6);

signed tmp;

avancer.data.id = 'V';
avancer.data.rtr = 0;
avancer.data.val = 48;
CanTxSendFrame(avancer);
consigne = 700;//682
CanTxSendFrame(fixerAngle);

vTaskDelay(1000);

    while(1){
        //xQueueReceive( xQueue_mesure, &( mesure ), ( TickType_t ) 10 );

        CanTxSendFrame(requestDistance);
        vTaskDelay(10);
        CanRxReadFrame(&responseDistance);
        mesure = responseDistance.data.val;
        erreur = (signed)consigne - mesure;
        if (mesure > 3000){
            erreur = erreur_precedente;
        }
        if(erreur > 50 ){
            avancer.data.val = 20;
            k = 1;
        }else if(erreur >=30 && erreur <50){
            avancer.data.val = 30;
            k = 1;
        }else {
            avancer.data.val = 40;
            k=1;
        }
        CanTxSendFrame(avancer);
        commande = Kp * erreur;
        printf("mesure %d, consigne %d, erreur %d\n", mesure,consigne, erreur);
        tmp = (signed)(-commande*k);
        //xQueueSend( xQueue_commande_angle_roues, &( tmp ), ( TickType_t ) 10 );
        cmdvitesseRot.data.val = (signed)(-commande*k);
        CanTxSendFrame(cmdvitesseRot);
        erreur_precedente = erreur;
        vTaskDelay(10);
    }

}

void timer1(void *p){
    static int j=0;
    j=(j)?0:1;
    led_j(j);

}


int main()
{
    HardwareInit(8);
    ControlPanelInit(7);
    //ControlPanelInitAddr("192.168.1.221",7);

    xSemaphore = xSemaphoreCreateMutex();


    TimerHandle_t htimer1;

    printf("Demarrage des taches\n");
    xTaskCreate(task_regulation_angle,"Tache 1",configMINIMAL_STACK_SIZE,NULL,7,&htask_1);
    xTaskCreate(task_regulation_distance,"Tache 2",configMINIMAL_STACK_SIZE,NULL,7,&htask_2);
    //xTaskCreate(recuperation_mesure,"Tache 3",configMINIMAL_STACK_SIZE,NULL,7,&htask_3);
    xTaskCreate(recuperation_angle,"Tache 4",configMINIMAL_STACK_SIZE,NULL,7,&htask_4);
    //xTaskCreate(commande_angle_roues,"Tache 5",configMINIMAL_STACK_SIZE,NULL,7,&htask_5);
    xTaskCreate(commande_angle_telemetre,"Tache 6",configMINIMAL_STACK_SIZE,NULL,7,&htask_6);
    htimer1=xTimerCreate("Timer 1",250,pdTRUE,NULL,timer1);
    xTimerStart(htimer1,0);
    vTaskStartScheduler();
    HardwareHalt();
    return 0;
}
